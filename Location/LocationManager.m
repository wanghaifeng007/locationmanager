//
//  LocationManager.m
//  LocatiionManager
//
//  Created by zzm on 15/8/24.
//  Copyright (c) 2015年 whfwudi. All rights reserved.
//

#import "LocationManager.h"

@implementation LocationManager
{
    CLLocationManager *locationManager;
    id<LocationManagerDelegate>delegate;
}

+(instancetype)sharedLoacationManager
{
    static LocationManager *mLocationManager;
    @synchronized(self)
    {
        if (mLocationManager == nil) {
            mLocationManager = [[LocationManager alloc] init];
        }
    }
    return mLocationManager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
            [locationManager requestAlwaysAuthorization];
            [locationManager requestWhenInUseAuthorization];
        }
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.distanceFilter  = kCLDistanceFilterNone;
    }
    
    return self;
}

- (void)startUpdateLocationWithLocationDelelgate:(id)locationDelegate
{
    delegate = locationDelegate;
    [locationManager startUpdatingLocation];
}

- (void)stopUpdateLocation
{
    [locationManager stopUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
            if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                [locationManager requestWhenInUseAuthorization];
            }
            break;
            
        default:
            break;
    }
}

//iOS 6以上
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *newLocation = locations.firstObject;
    CLLocationCoordinate2D oldCoordinate = newLocation.coordinate;
    
    /*
     *after iOS 5.0
     */
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        for (CLPlacemark *place in placemarks) {
            if (placemarks.count > 0) {
                [self stopUpdateLocation];
                LocationItem *locationItem = [[LocationItem alloc] init];
                locationItem.name = place.name;
                locationItem.thoroughfare = place.thoroughfare;
                locationItem.subThoroughfare = place.subThoroughfare;
                locationItem.locality = place.locality;
                locationItem.subLocality = place.subLocality;
                locationItem.country = place.country;
                locationItem.longitude = oldCoordinate.longitude;
                locationItem.latitude = oldCoordinate.latitude;
                
                [delegate locationFinished:locationItem];
                }else{
                NSLog(@"你是不是住在火星上");
            }
        }
    }];
}

//iOS 6
-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    /*
     *after iOS 5.0
     */
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        
        for (CLPlacemark *place in placemarks) {
            if (placemarks.count > 0) {
                [self stopUpdateLocation];
                LocationItem *locationItem = [[LocationItem alloc] init];
                locationItem.name = place.name;
                locationItem.thoroughfare = place.thoroughfare;
                locationItem.subThoroughfare = place.subThoroughfare;
                locationItem.locality = place.locality;
                locationItem.subLocality = place.subLocality;
                locationItem.country = place.country;
                locationItem.longitude = newLocation.coordinate.longitude;
                locationItem.latitude = newLocation.coordinate.latitude;
                
                [delegate locationFinished:locationItem];

            }else{
                NSLog(@"你是不是住在火星上");
            }
        }
    }];
}


-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    switch (error.code) {
        case kCLErrorLocationUnknown:
            NSLog(@"location is currently unknown, but CL will keep trying");
            break;
        case kCLErrorDenied:
            NSLog(@"Access to location or ranging has been denied by the user");
            break;
        case kCLErrorNetwork:
            NSLog(@"general, network-related error");
            break;
        default:
            NSLog(@"other error place look api doc");
            break;
    }
}

@end
