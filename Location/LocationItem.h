//
//  LocationItem.h
//  LocatiionManager
//
//  Created by zzm on 15/8/24.
//  Copyright (c) 2015年 whfwudi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationItem : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *thoroughfare;
@property (nonatomic, strong) NSString *subThoroughfare;
@property (nonatomic, strong) NSString *locality;
@property (nonatomic, strong) NSString *subLocality;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, assign) CLLocationDegrees longitude; //经度
@property (nonatomic, assign) CLLocationDegrees latitude;  //纬度

@end
