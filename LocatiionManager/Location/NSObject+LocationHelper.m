//
//  NSObject+LocationHelper.m
//  LocatiionManager
//
//  Created by zzm on 15/11/5.
//  Copyright © 2015年 whfwudi. All rights reserved.
//

#import "NSObject+LocationHelper.h"
#import <CoreLocation/CoreLocation.h>

@implementation NSObject (LocationHelper)

+ (BOOL)isOpenLocationService
{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    if (kCLAuthorizationStatusDenied == status || kCLAuthorizationStatusRestricted == status) {
        return FALSE;
    }else{
        return TRUE;
    }
}

@end
