//
//  LocationManager.h
//  LocatiionManager
//
//  Created by zzm on 15/8/24.
//  Copyright (c) 2015年 whfwudi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "LocationItem.h"

typedef void (^ErrorBlock)(NSString *responseString);
typedef void (^LocationCompleteBlock)(LocationItem *locationItem);

@protocol LocationManagerDelegate <NSObject>
@required

- (void)locationFinished:(LocationItem *)locationItem;

@end

@interface LocationManager : NSObject<CLLocationManagerDelegate>

+ (instancetype)sharedLoacationManager;

- (void)startUpdateLocationWithLocationDelelgate:(id<LocationManagerDelegate>)locationDelegate;

@end
