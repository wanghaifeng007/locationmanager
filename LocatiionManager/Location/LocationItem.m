//
//  LocationItem.m
//  LocatiionManager
//
//  Created by zzm on 15/8/24.
//  Copyright (c) 2015年 whfwudi. All rights reserved.
//

#import "LocationItem.h"

@implementation LocationItem
@synthesize name;
@synthesize thoroughfare;
@synthesize subThoroughfare;
@synthesize locality;
@synthesize subLocality;
@synthesize country;
@synthesize latitude;
@synthesize longitude;

@end
