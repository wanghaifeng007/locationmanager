//
//  AppDelegate.h
//  LocatiionManager
//
//  Created by zzm on 15/8/24.
//  Copyright (c) 2015年 whfwudi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

