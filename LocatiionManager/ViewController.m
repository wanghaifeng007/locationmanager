//
//  ViewController.m
//  LocatiionManager
//
//  Created by zzm on 15/8/24.
//  Copyright (c) 2015年 whfwudi. All rights reserved.
//

#import "ViewController.h"
#import "LocationManager.h"
#import "NSObject+LocationHelper.h"

@interface ViewController ()<LocationManagerDelegate>

@end

@implementation ViewController
@synthesize locationLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    if ([NSObject isOpenLocationService]) {
        [[LocationManager sharedLoacationManager] startUpdateLocationWithLocationDelelgate:self];
    }else{
        NSLog(@"没有权限");
        UIApplication *app = [UIApplication sharedApplication];
        NSURL *settingsURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        if ([app canOpenURL:settingsURL]) {
            [app openURL:settingsURL];
        }
    }
}

-(void)locationFinished:(LocationItem *)locationItem
{
    locationLabel.text = locationItem.name;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
